'use strict'

tempApp.filter('minimum', [ function() {
    return function(arrTemp, minimum) {
      var arrReturn = new Array();
      var min = minimum ? minimum : 15;
      angular.forEach(arrTemp, function(value, key){
        if(value.temp>=min) arrReturn.push(value);
      });
      return arrReturn;
    }
  }]);