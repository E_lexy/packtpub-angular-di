'use strict';


var currentCtrl = function ($scope, reading) {
	$scope.temp = 17;

	$scope.good_idea = reading.version;

	$scope.save = function () {
		reading.save($scope.temp);
	}
};

currentCtrl.$inject = ['$scope', 'reading'];

tempApp.controller('currentCtrl', currentCtrl);

tempApp.controller('HistoryCtrl', ['$scope', 'reading',
	function ($scope, reading) {
		reading.query(function (data) {
			$scope.historyData = data;
			$scope.$apply();
		});

		//below unchanged
		$scope.tempMin = 15;
	}]);
