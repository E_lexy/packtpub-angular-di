'use strict';

// Declare app level module which depends on filters, and services
var tempApp = angular.module('tempApp', ['serviceModule']).
  config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/current', {templateUrl: 'partials/current.html', controller: 'currentCtrl'});
    $routeProvider.when('/history', {templateUrl: 'partials/history.html', controller: 'HistoryCtrl'});
    $routeProvider.when('/transclude', {templateUrl: 'partials/transclude.html', controller: 'TranscludeCtrl'});
    $routeProvider.otherwise({redirectTo: '/current'});
  }]);