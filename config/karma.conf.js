basePath = '../';

files = [
  JASMINE,
  JASMINE_ADAPTER,
  'test/lib/angular.js',
  'test/lib/angular-*.js',
  'test/lib/angular-mocks.js',

  'app/js/*.js',
  'test/spec/app/**/*.js'
];

autoWatch = true;

browsers = ['Chrome'];

junitReporter = {
  outputFile: 'test_out/unit.xml',
  suite: 'unit'
};
