'use strict';

describe('tempApp', function () {

	beforeEach(function () {
		browser().navigateTo('../index.html');
	});

	it('should automatically redirect to /current when location hash/fragment is empty', function () {
		expect(browser().location().url()).toBe("/current");
	});

	describe('currentCtrl', function () {

		beforeEach(function () {
			browser().navigateTo('#/current');
		});


		it('should render current when user navigates to /current', function () {
			expect(element('[ng-view] span:first').text()).
					toMatch(/Current temperature/);
		});

		it('should show the "Not too bad" comment on high temperatures', function () {
			input('temp').enter('20');
			expect(element('[ng-view] p').text()).
					toMatch(/Not too bad/);
		});

		it('should show the "Kinda chilly" comment on low temperatures', function () {
			input('temp').enter('12');
			expect(element('[ng-view] p').text()).
					toMatch(/is a little chilly/);
		});

	});
//
//
//	describe('view2', function() {
//
//		beforeEach(function() {
//			browser().navigateTo('#/view2');
//		});
//
//
//		it('should render view2 when user navigates to /view2', function() {
//			expect(element('[ng-view] p:first').text()).
//					toMatch(/partial for view 2/);
//		});
//
//	});
});