'use strict';

var scope;

describe('HistoryController', function () {

	beforeEach(function () {
		module('tempApp');
	});

	beforeEach(module(function ($provide) {
		$provide.service('reading', [ function (Project) {
			this.query = function () {
				return [
					{ "date": "2013-04-01T17:01:22.634Z", "temp": 8},
					{ "date": "2013-04-02T17:01:22.634Z", "temp": 13},
					{ "date": "2013-04-03T17:01:22.634Z", "temp": 15},
					{ "date": "2013-04-04T17:01:22.634Z", "temp": 11}
				];
			};
		}]);
	}));

	beforeEach(inject(function ($rootScope, $controller, reading) {
		scope = $rootScope.$new();
		$controller('HistoryCtrl', {
			$scope: scope
		});
	}));

	describe('tempMin', function () {
		it('should be defined', function () {
			expect(scope.tempMin).toBeDefined();
		});
	})
});
